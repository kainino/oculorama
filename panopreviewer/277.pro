QT += core widgets

TARGET = 277
TEMPLATE = app
CONFIG += c++11
CONFIG += warn_on

INCLUDEPATH += include

include(src/src.pri)

RESOURCES += glsl.qrc

# Enable console on Windows so that we can actually see debug prints.
win32 {
    CONFIG += console
}

*-clang*|*-g++* {
    message("Enabling additional warnings")
    CONFIG -= warn_on
    QMAKE_CXXFLAGS += -Wall -Wextra -pedantic -Winit-self
    QMAKE_CXXFLAGS += -Wno-strict-aliasing
    QMAKE_CXXFLAGS += -Wno-pedantic
    QMAKE_CXXFLAGS += -fno-omit-frame-pointer
}
*-clang* {
    QMAKE_CXXFLAGS += -Wno-unneeded-internal-declaration
}
linux-clang*|linux-g++*|macx-clang*|macx-g++* {
    message("Enabling stack protector")
    QMAKE_CXXFLAGS += -fstack-protector-all
}

# FOR LINUX & MAC USERS INTERESTED IN ADDITIONAL BUILD TOOLS
# ----------------------------------------------------------
# This conditional exists to enable Address Sanitizer (ASAN) during
# the automated build. ASAN is a compiled-in tool which checks for
# memory errors (like Valgrind). You may use these tools yourself;
# `asan-build.sh` builds the project with ASAN. But be aware: ASAN may
# trigger a lot of false-positive leak warnings for the Qt libraries.
# (Use `asan-run.sh` to run the program with leak checking disabled.)
address_sanitizer {
    message("Enabling Address Sanitizer")
    QMAKE_CXXFLAGS += -fsanitize=address
    QMAKE_LFLAGS += -fsanitize=address
}

INCLUDEPATH += $$PWD/ovr_sdk/LibOVR/Include
INCLUDEPATH += $$PWD/ovr_sdk/LibOVRKernel/Src
linux {
    QT += x11extras
    LIBS           += -L$$PWD/ovr_sdk/LibOVR/Lib/Linux/x86_64/Release/ -lOVR
    PRE_TARGETDEPS +=   $$PWD/ovr_sdk/LibOVR/Lib/Linux/x86_64/Release/libOVR.a
    LIBS += -lSM -lICE -lX11 -lXext -lpthread -lrt -lXrandr -ludev -ldl
}
win32 {
    # ONLY works on 64-bit Qt VS2013 build right now.
    LIBS           += -L$$PWD/ovr_sdk\LibOVR\Lib\Windows\x64\Release\VS2013 -lLibOVR
    PRE_TARGETDEPS +=   $$PWD/ovr_sdk\LibOVR\Lib\Windows\x64\Release\VS2013\LibOVR.lib
    LIBS += -lwinmm -lws2_32
}
macx {
    LIBS           += -F$$PWD/ovr_sdk/LibOVR/Lib/Mac/Release/ -framework LibOVR
    PRE_TARGETDEPS +=  $$PWD/ovr_sdk/LibOVR/Lib/Mac/Release/LibOVR.framework
    LIBS           += -framework CoreAudio -framework OpenAL -framework CoreFoundation -framework ApplicationServices -framework OpenGL -framework IOKit -framework CoreServices -framework Cocoa -framework Carbon
}
