#pragma once

#include <utils.h>

#include <QOpenGLFunctions_3_2_Core>

#include <OVR_CAPI.h>
#include <OVR_CAPI_GL.h>
#include <OVR_Kernel.h>


/**
 * Largely copied from http://nuclear.mutantstargoat.com/hg/oculus2/
 */
class Oculus
{
private:
    ovrHmd hmd;
    ovrSizei eye_res[2];
    ovrGLTexture fb_ovr_tex[2];
    ovrGLConfig glcfg;
    ovrEyeRenderDesc eye_rdesc[2];
    unsigned int hmd_caps, distort_caps;
    ovrPosef pose[2];

    int fb_width, fb_height;
    int fb_tex_width, fb_tex_height;
    GLuint fbo, fb_tex, fb_depth;

    QOpenGLFunctions_3_2_Core gl;

public:
    Oculus();

    void destroy();

    ovrFrameTiming begin_frame();
    void end_frame();
    void begin_eye(int which, glm::mat4 &proj, glm::mat4 &view);

    void update_render_target();

    void dismiss_hsw();

    void debug_print();
    void debug_print_loop();
};
