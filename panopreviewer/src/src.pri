INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

SOURCES += \
    $$PWD/main.cpp \
    $$PWD/openglwindow.cpp \
    $$PWD/mygl.cpp \
    $$PWD/utils.cpp \
    $$PWD/oculus_native.cpp \
    $$PWD/oculus.cpp \
    $$PWD/panorama.cpp \
    $$PWD/panoshader.cpp \
    $$PWD/panoppm.cpp \
    $$PWD/panoset.cpp

HEADERS += \
    $$PWD/openglwindow.h \
    $$PWD/mygl.h \
    $$PWD/utils.h \
    $$PWD/oculus_native.h \
    $$PWD/oculus.h \
    $$PWD/panorama.h \
    $$PWD/panoshader.h \
    $$PWD/panoppm.h \
    $$PWD/panoset.h
