// Handle the glfw native objects WITHOUT exposing glfw3native include
#pragma once

#include <utils.h>

#include <OVR_CAPI.h>
#include <OVR_CAPI_GL.h>


void createGLConfig(ovrHmd &hmd, ovrGLConfig &glcfg);
