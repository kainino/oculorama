#include <mygl.h>

#include <QApplication>
#include <QSurfaceFormat>
#include <QDebug>
#include <QTimer>

void debugFormatVersion()
{
    QSurfaceFormat form = QSurfaceFormat::defaultFormat();
    QSurfaceFormat::OpenGLContextProfile prof = form.profile();

    const char *profile =
        prof == QSurfaceFormat::CoreProfile ? "Core" :
        prof == QSurfaceFormat::CompatibilityProfile ? "Compatibility" :
        "None";

    printf("Requested format:\n");
    printf("  Version: %d.%d\n", form.majorVersion(), form.minorVersion());
    printf("  Profile: %s\n", profile);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // Set OpenGL 3.2 and multisampling
    QSurfaceFormat format;
    format.setVersion(3, 2);
    format.setOption(QSurfaceFormat::DeprecatedFunctions, false);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setDepthBufferSize(24);
    format.setSamples(2);  // Uncomment for nice antialiasing. Not always supported.

    QSurfaceFormat::setDefaultFormat(format);
    debugFormatVersion();

    MyGL w;
    w.setMinimumWidth(960);
    w.setMinimumHeight(540);
    w.show();

    return a.exec();
}
