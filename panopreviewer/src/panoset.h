#pragma once

#include <QSet>

#include <panorama.h>


class PanoSet
{
private:
    QSet<Panorama *> _panos;

public:
    PanoSet();
    ~PanoSet();

    inline QSet<Panorama *> &panos() {
        return _panos;
    }

    void load_create(const QString &filename);
};
