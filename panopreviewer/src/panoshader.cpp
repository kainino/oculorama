#include "panoshader.h"


void PanoShader::create(const char *vertfile, const char *geomfile, const char *fragfile)
{
    prog.addShaderFromSourceFile(QOpenGLShader::Vertex  , vertfile);
    prog.addShaderFromSourceFile(QOpenGLShader::Geometry, geomfile);
    prog.addShaderFromSourceFile(QOpenGLShader::Fragment, fragfile);
    prog.link();

    attrTexCoord  = prog.attributeLocation("vs_TexCoord");

    unifModel     = prog.uniformLocation("Model");
    unifViewProj  = prog.uniformLocation("ViewProj");
    unifAspect    = prog.uniformLocation("Aspect");
    unifPanoDepth = prog.uniformLocation("PanoDepth");
    unifPanoColor = prog.uniformLocation("PanoColor");
    unifClipDist  = prog.uniformLocation("ClipDist");
    unifCamPos    = prog.uniformLocation("CamPos");
    unifSmartDist = prog.uniformLocation("SmartDist");
    unifCenter    = prog.uniformLocation("Center");
}

void PanoShader::draw(QOpenGLFunctions &f, Drawable &d, const glm::vec3 &campos, float clipdist, float smartdist)
{
    prog.bind();

    // Each of the following blocks checks that:
    //   * This shader has this attribute, and
    //   * This Drawable has a vertex buffer for this attribute.
    // If so, it binds the appropriate buffers to each attribute.

    if (attrTexCoord != -1 && d.bindTexCoord()) {
        prog.enableAttributeArray(attrTexCoord);
        f.glVertexAttribPointer(attrTexCoord, 2, GL_FLOAT, false, 0, NULL);
    }

    static const GLuint TEX0 = 0;
    d.bindDepthTex(TEX0);
    prog.setUniformValue(unifPanoDepth, TEX0);

    static const GLuint TEX1 = 1;
    d.bindColorTex(TEX1);
    prog.setUniformValue(unifPanoColor, TEX1);

    if (unifCamPos >= 0) prog.setUniformValue(unifCamPos, la::to_qvec3(campos));

    if (unifClipDist >= 0) prog.setUniformValue(unifClipDist, clipdist);
    if (unifSmartDist >= 0) prog.setUniformValue(unifSmartDist, smartdist);

    // Bind the index buffer and then draw shapes from it.
    // This invokes the shader program, which accesses the vertex buffers.
    d.bindIdx();
    f.glDrawElements(d.drawMode(), d.elemCount(), GL_UNSIGNED_INT, 0);

    if (attrTexCoord != -1) prog.disableAttributeArray(attrTexCoord);

    printGLErrorLog();
}
