#include "panorama.h"
#include <utils.h>

#include <cstdio>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QImage>

#include <panoppm.h>


QMap<QPair<int, int>, Panorama::Bufs> Panorama::buffers = QMap<QPair<int, int>, Panorama::Bufs>();

Panorama::Panorama()
    : texDepth(QOpenGLTexture::Target2D)
    , texColor(QOpenGLTexture::Target2D)
{
}

Panorama::~Panorama()
{
}

void Panorama::create()
{
    if (texDepth.create()) {
        texDepth.bind();
        texDepth.setMinMagFilters(QOpenGLTexture::Linear, QOpenGLTexture::Linear);
        texDepth.setWrapMode(QOpenGLTexture::Repeat);
    }

    if (texColor.create()) {
        texColor.bind();
        texColor.setMinMagFilters(QOpenGLTexture::Linear, QOpenGLTexture::Linear);
        texColor.setWrapMode(QOpenGLTexture::Repeat);
    }
}

void Panorama::destroy()
{
    // TODO actually remove things from static QMap somehow?
}

double remap(double t, double t1, double t2, double u1, double u2)
{
    return (t - t1) / (t2 - t1) * (u2 - u1) + u1;
}

void Panorama::loadJSON(const QJsonObject &json, const QString &basepath)
{
    QString dfile  = json["depth"].toString();
    QString cfile  = json["color"].toString();
    float permeter = json["permeter"].toDouble();
    auto rota      = json["rotation"].toArray();
    auto transa    = json["translation"].toArray();
    auto mesh_w    = json["resolution"].toArray()[0].toInt();
    auto mesh_h    = json["resolution"].toArray()[1].toInt();
    glm::quat rotation(
            rota[0].toDouble(),
            rota[1].toDouble(),
            rota[2].toDouble(),
            rota[3].toDouble());
    glm::vec3 translation(
            transa[0].toDouble(),
            transa[1].toDouble() + HEADHEIGHT,
            transa[2].toDouble());
    _translation = translation;
    modelmat = glm::translate(glm::mat4(), translation) * glm::mat4(rotation);

    PanoPPM ppm(basepath + "/" + dfile, permeter);
    int width = ppm.width();
    int height = ppm.height();
    aspect = height / (float) width;
    dim = qMakePair(mesh_w, mesh_h);

    texDepth.setSize(width, height);
    texDepth.setFormat(QOpenGLTexture::R32F);
    texDepth.allocateStorage();
    texDepth.setData(QOpenGLTexture::Red, QOpenGLTexture::Float32, ppm.data().data());

    QImage cim(basepath + "/" + cfile);
    if (cim.isNull()) {
        printf("Color image loading failed\n");
        exit(EXIT_FAILURE);
    }
    texColor.setData(cim, QOpenGLTexture::DontGenerateMipMaps);

    if (!buffers.contains(dim)) {
        std::vector<GLuint> is;
        std::vector<glm::vec2> ts;

        for (int y = 0; y < mesh_h; y++) {
            for (int x = 0; x <= mesh_w; x++) {
                ts.push_back(glm::vec2(x / (float) mesh_w,
                                       y / (mesh_h - 1.0f)));
            }
        }

        for (int y = 0; y < mesh_h - 1; y++) {
            for (int x = 0; x < mesh_w; x++) {
                int w1 = mesh_w + 1;
                is.push_back((y + 0) * w1 + (x + 0));
                is.push_back((y + 0) * w1 + (x + 1));
                is.push_back((y + 1) * w1 + (x + 0));

                is.push_back((y + 1) * w1 + (x + 0));
                is.push_back((y + 0) * w1 + (x + 1));
                is.push_back((y + 1) * w1 + (x + 1));
            }
        }
        GLuint count = (GLuint) is.size();

        QOpenGLBuffer bufIdx(QOpenGLBuffer::IndexBuffer);
        QOpenGLBuffer bufTexCoord(QOpenGLBuffer::VertexBuffer);

        if (bufIdx.create() && bufIdx.bind()) {
            bufIdx.setUsagePattern(QOpenGLBuffer::StaticDraw);
        }

        if (bufTexCoord.create() && bufTexCoord.bind()) {
            bufTexCoord.setUsagePattern(QOpenGLBuffer::StaticDraw);
        }

        bufIdx.bind();
        bufIdx.allocate(is.data(), (int) is.size() * sizeof(GLuint));
        bufTexCoord.bind();
        bufTexCoord.allocate(ts.data(), (int) ts.size() * sizeof(glm::vec2));

        Bufs &bs = buffers[dim];
        bs.count = count;
        bs.idx = bufIdx;
        bs.txc = bufTexCoord;
    }

    bufs = &buffers[dim];

    printGLErrorLog();
}

void Panorama::setUniforms(PanoShader &ps)
{
    if (ps.unifModel  >= 0) ps.prog.setUniformValue(ps.unifModel , la::to_qmat(modelmat));
    if (ps.unifAspect >= 0) ps.prog.setUniformValue(ps.unifAspect, aspect);
    if (ps.unifCenter >= 0) ps.prog.setUniformValue(ps.unifCenter, la::to_qvec3(_translation));

    printGLErrorLog();
}

GLenum Panorama::drawMode()
{
    return GL_TRIANGLES;
}

int Panorama::elemCount()
{
    return bufs->count;
}

bool Panorama::bindIdx()
{
    return bufs->idx.bind();
}

bool Panorama::bindTexCoord()
{
    return bufs->txc.bind();
}

void Panorama::bindDepthTex(GLuint unit)
{
    texDepth.bind(unit);
}

void Panorama::bindColorTex(GLuint unit)
{
    texColor.bind(unit);
}
