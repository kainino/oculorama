#include <cstdio>
#include <iostream>
#include <thread>
#include <chrono>

#include "oculus.h"
#include "oculus_native.h"

#include <utils.h>


using namespace OVR;

Oculus::Oculus()
    : fbo(0)
{
    gl.initializeOpenGLFunctions();

    ovr_Initialize();
    this->hmd = ovrHmd_Create(0);
    if (!this->hmd) {
        printf("ovrHmd_Create failed; using debug HMD\n");
        this->hmd = ovrHmd_CreateDebug(ovrHmd_DK2);
    }
    if (!this->hmd) {
        printf("Failed to create debug HMD\n");
        exit(EXIT_FAILURE);
    }

    auto tracking_caps = 0
        | ovrTrackingCap_Orientation
        | ovrTrackingCap_MagYawCorrection
        | ovrTrackingCap_Position
        ;
    ovrHmd_ConfigureTracking(this->hmd, tracking_caps, 0);

    eye_res[0] = ovrHmd_GetFovTextureSize(hmd, ovrEye_Left , hmd->DefaultEyeFov[0], 1.0);
    eye_res[1] = ovrHmd_GetFovTextureSize(hmd, ovrEye_Right, hmd->DefaultEyeFov[1], 1.0);

    fb_width = eye_res[0].w + eye_res[1].w;
    fb_height = std::max(eye_res[0].h, eye_res[1].h);
    this->update_render_target();

    createGLConfig(hmd, glcfg);

    // TODO: If we want direct-hmd mode, do that here

    /* enable low-persistence display and dynamic prediction for lattency compensation */
    //hmd_caps = ovrHmdCap_LowPersistence | ovrHmdCap_DynamicPrediction;
    ovrHmd_SetEnabledCaps(hmd, hmd_caps);

    /* configure SDK-rendering and enable chromatic abberation correction, vignetting, and
     * timewrap, which shifts the image before drawing to counter any lattency between the call
     * to ovrHmd_GetEyePose and ovrHmd_EndFrame.
     */
    distort_caps = 0
        | ovrDistortionCap_TimeWarp
        | ovrDistortionCap_Vignette
        //| ovrDistortionCap_NoRestore
        | ovrDistortionCap_Overdrive
        //| ovrDistortionCap_HqDistortion
        //| ovrDistortionCap_TimewarpJitDelay
        ;
    if (!ovrHmd_ConfigureRendering(hmd, &glcfg.Config, distort_caps, hmd->DefaultEyeFov, eye_rdesc)) {
        fprintf(stderr, "failed to configure distortion renderer\n");
    }

    // Disable health and safety warning
    //ovrhmd_EnableHSWDisplaySDKRender(hmd, 0);
}

void Oculus::destroy()
{
    ovrHmd_Destroy(hmd);
    ovr_Shutdown();
    printf("Shut down.\n");
}

ovrFrameTiming Oculus::begin_frame()
{
    ovrFrameTiming t = ovrHmd_BeginFrame(hmd, 0);
    gl.glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    return t;
}

void Oculus::end_frame()
{
    gl.glBindFramebuffer(GL_FRAMEBUFFER, 0);
    ovrHmd_EndFrame(hmd, pose, &fb_ovr_tex[0].Texture);
    gl.glUseProgram(0);
}

void Oculus::begin_eye(int which, glm::mat4 &proj, glm::mat4 &view)
{
    ovrEyeType eye = hmd->EyeRenderOrder[which];
    glViewport(eye == ovrEye_Left ? 0 : fb_width / 2, 0, fb_width / 2, fb_height);

    ovrMatrix4f pj;
    pj = ovrMatrix4f_Projection(hmd->DefaultEyeFov[eye], 0.05f, 200.0f, 1);

    pose[eye] = ovrHmd_GetHmdPosePerEye(hmd, eye);

    ovrPosef &p = pose[eye];
    ovrEyeRenderDesc &rd = eye_rdesc[eye];
    glm::quat poseq = glm::quat(
            -p.Orientation.w, p.Orientation.x, p.Orientation.y, p.Orientation.z);
    glm::vec3 posep = glm::vec3(
            -p.Position.x,
            -p.Position.y - ovrHmd_GetFloat(hmd, OVR_KEY_EYE_HEIGHT, 1.65f),
            -p.Position.z);
    glm::vec3 rdescp = glm::vec3(
            rd.HmdToEyeViewOffset.x,
            rd.HmdToEyeViewOffset.y,
            rd.HmdToEyeViewOffset.z);

    proj = glm::transpose(glm::make_mat4(&pj.M[0][0]));
    view = glm::translate(glm::mat4(), rdescp)
         * glm::mat4_cast(poseq)
         * glm::translate(glm::mat4(), posep);
}

void Oculus::update_render_target()
{
    if (!fbo) {
        gl.glGenFramebuffers(1, &fbo);
        gl.glGenTextures(1, &fb_tex);
        gl.glGenRenderbuffers(1, &fb_depth);

        gl.glBindTexture(GL_TEXTURE_2D, fb_tex);
        gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

    gl.glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    fb_tex_width  = next_pow2(fb_width);
    fb_tex_height = next_pow2(fb_height);

    gl.glBindTexture(GL_TEXTURE_2D, fb_tex);
    gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, fb_tex_width, fb_tex_height, 0,
            GL_RGBA, GL_UNSIGNED_BYTE, 0);
    gl.glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fb_tex, 0);

    /* create and attach the renderbuffer that will serve as our z-buffer */
    gl.glBindRenderbuffer(GL_RENDERBUFFER, fb_depth);
    gl.glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, fb_tex_width, fb_tex_height);
    gl.glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, fb_depth);

    if (gl.glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        fprintf(stderr, "incomplete framebuffer!\n");
    }

    gl.glBindFramebuffer(GL_FRAMEBUFFER, 0);

    for (int i = 0; i < 2; ++i) {
        fb_ovr_tex[i].OGL.Header.API = ovrRenderAPI_OpenGL;
        fb_ovr_tex[i].OGL.Header.TextureSize.w = fb_tex_width;
        fb_ovr_tex[i].OGL.Header.TextureSize.h = fb_tex_height;
        /* this next field is the only one that differs between the two eyes */
        fb_ovr_tex[i].OGL.Header.RenderViewport.Pos.x = i == 0 ? 0 : fb_width / 2;
        fb_ovr_tex[i].OGL.Header.RenderViewport.Pos.y = 0;
        fb_ovr_tex[i].OGL.Header.RenderViewport.Size.w = fb_width / 2;
        fb_ovr_tex[i].OGL.Header.RenderViewport.Size.h = fb_height;
        fb_ovr_tex[i].OGL.TexId = fb_tex;   /* both eyes will use the same texture id */
    }
}

void Oculus::dismiss_hsw()
{
    ovrHmd_DismissHSWDisplay(hmd);
}

void Oculus::debug_print()
{
    auto frame_timing = ovrHmd_BeginFrameTiming(hmd, 0);
    ovrTrackingState ts = ovrHmd_GetTrackingState(hmd, frame_timing.ScanoutMidpointSeconds);

    if (ts.StatusFlags & (ovrStatus_OrientationTracked | ovrStatus_PositionTracked)) {
        Posef pose = ts.HeadPose.ThePose;
        float yaw, pitch, roll;
        pose.Rotation.GetEulerAngles<Axis_Y, Axis_X, Axis_Z>(&yaw, &pitch, &roll);

        std::cout << "yaw: "   << RadToDegree(yaw  ) << std::endl;
        std::cout << "pitch: " << RadToDegree(pitch) << std::endl;
        std::cout << "roll: "  << RadToDegree(roll ) << std::endl;

        ovrHmd_EndFrameTiming(hmd);
    } else {
        printf("debug_print %i %i\n",
                ts.StatusFlags & ovrStatus_OrientationTracked,
                ts.StatusFlags & ovrStatus_PositionTracked
              );
    }
}

void Oculus::debug_print_loop()
{
    while (hmd) {
        this->debug_print();

        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}
