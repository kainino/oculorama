#include "oculus_native.h"

#include <cstring>

#if defined(OVR_OS_WIN32)
// TODO
#include <QWindow>
#elif defined(OVR_OS_MAC)
// TODO
#elif defined(OVR_OS_LINUX)
#include <QtX11Extras/QX11Info>
#endif


void createGLConfig(ovrHmd &hmd, ovrGLConfig &glcfg)
{
    memset(&glcfg, 0, sizeof(glcfg));
    glcfg.OGL.Header.API = ovrRenderAPI_OpenGL;
    glcfg.OGL.Header.BackBufferSize = hmd->Resolution;
    glcfg.OGL.Header.Multisample = 0;
#if defined(OVR_OS_WIN32)
    // TODO
    glcfg.OGL.Window = (HWND) window_id;
    glcfg.OGL.DC = GetDC(glcfg.OGL.Window);
#elif defined(OVR_OS_MAC)
    // Don't know which of these to use
    // TODO
    //glcfg.OGL.Win = glfwGetCocoaWindow(window);
    //glcfg.OGL.Window = glfwGetCocoaWindow(window);
#elif defined(OVR_OS_LINUX)
    glcfg.OGL.Disp = QX11Info::display();
    //glcfg.OGL.Win = glfwGetX11Window(window);
#endif
}
