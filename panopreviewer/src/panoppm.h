#pragma once

#include <QVector>


class PanoPPM
{
private:
    int _width;
    int _height;
    QVector<float> _data;

public:
    explicit PanoPPM(const QString &filename, float permeter);

    inline int width() const { return _width; }
    inline int height() const { return _height; }

    inline const QVector<float> &data() const { return _data; }
};
