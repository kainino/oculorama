#include "panoset.h"

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QFileInfo>
#include <QDir>

PanoSet::PanoSet()
{

}

PanoSet::~PanoSet()
{
    for (Panorama *pano : _panos) {
        pano->destroy();
        delete pano;
    }
}

static QJsonArray loadJSONArray(const QString &filename)
{
    QFile jsonfile(filename);
    if (!jsonfile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open JSON file.");
        return QJsonArray();
    }
    QByteArray jsondata = jsonfile.readAll();
    QJsonParseError error;
    QJsonDocument json = QJsonDocument::fromJson(jsondata, &error);
    if (error.error != QJsonParseError::NoError) {
        printf("JSON parse failure: %s\n", error.errorString().toStdString().c_str());
        exit(EXIT_FAILURE);
    }
    return json.array();
}

void PanoSet::load_create(const QString &filename)
{
    QJsonArray json = loadJSONArray(filename);

    QFileInfo info(filename);
    QDir dir = info.dir();

    for (auto j : json) {
        Panorama *pano = new Panorama();
        _panos.insert(pano);
        pano->create();
        pano->loadJSON(j.toObject(), dir.path());
        printf("Loading panorama: %s\n", j.toObject()["depth"].toString().toStdString().c_str());
    }
}
