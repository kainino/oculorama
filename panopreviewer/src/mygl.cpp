#include "mygl.h"

#include <iostream>
#include <QApplication>
#include <QKeyEvent>


MyGL::MyGL()
    : OpenGLWindow()
    , ocu(nullptr)
    , cameraxyz(8.5, 0, 1.5)
    , camerasph(20, 0, 5.7)
    , captured(false)
#if TIMING
    , rendertiming(false)
#endif
{
    setAnimating(true);
}

MyGL::~MyGL()
{
    vao.destroy();
}

void MyGL::initialize()
{
    window_id = this->winId();
    // Print out some information about the current OpenGL context
    debugContextVersion();

    // Set a few settings/modes
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_FASTEST);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_FASTEST);
    // Set the point rendering size
    glPointSize(2);
    // Set the color which clears the screen between frames
    glClearColor(0.6f, 0.7f, 0.9f, 1.0f);

    printGLErrorLog();

    // Create a VAO
    vao.create();
    // We have to have a VAO bound in OpenGL 3.2 Core. But if we're not
    // using multiple VAOs, we can just bind one once.
    vao.bind();

    // Create a panorama
    QStringList args = QApplication::arguments();
    if (args.size() >= 2) {
        panos.load_create(args[1]);
        std::cerr << "Panorama loaded." << std::endl;
    } else {
        std::cerr << "Usage: " << args[0].toUtf8().constData() << " JSON_FILE" << std::endl;
    }

    // Create and set up the wireframe shader
    prog.create(":/glsl/pano.vert.glsl",
                ":/glsl/pano.geom.glsl",
                ":/glsl/pano.frag.glsl");

    updateCamera();
    printGLErrorLog();
}

void MyGL::resizeEvent(QResizeEvent *)
{
    if (valid()) {
        updateCamera();
        printGLErrorLog();
    }
}

void MyGL::render()
{
#if TIMING
    auto t1 = QTime::currentTime();
#endif

    updateKeys();

    ovrFrameTiming timing;

    if (ocu) {
        ocu->dismiss_hsw();

        glm::mat4 proj, view;

        timing = ocu->begin_frame();

        glm::mat4 manual
            = glm::eulerAngleXY(0.f, -camerasph.z)
            * glm::translate(glm::mat4(), cameraxyz * glm::vec3(-1, 1, -1));

        ocu->begin_eye(0, proj, view);
        setViewProj(proj * view * manual);
        renderFrame();

        ocu->begin_eye(1, proj, view);
        setViewProj(proj * view * manual);
        renderFrame();

        ocu->end_frame();
    } else {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        renderFrame();
        swapBuffers();
    }

    printf("%s %s\n", glm::to_string(cameraxyz).c_str(), glm::to_string(camerasph).c_str());

#if TIMING
    glFlush();
    auto t2 = QTime::currentTime();
    if (rendertiming) {
        printf("\rRefresh time: %3d   Render time: %3d   Oculus time: %3d   ",
               lastframe.msecsTo(t1), t1.msecsTo(t2), (int) (ocu ? timing.DeltaSeconds * 1000 : 0));
    }
    lastframe = t1;
#endif

    printGLErrorLog();
}

void MyGL::renderFrame()
{
    prog.prog.bind();
    for (Panorama *pano : panos.panos()) {
        if (glm::distance(pano->translation(), cameraxyz) < PANODISTLIMIT) {
            pano->setUniforms(prog);
            prog.draw(*this, *pano, cameraxyz, CLIPDIST, SMARTDIST);
        }
    }
}

void MyGL::updateKeys()
{
    bool update = false;
    if (keys.contains(Qt::Key_A)) {
        cameraxyz += glm::rotateY(glm::vec3(-FLYSPEED, 0, 0), camerasph.z);
        update = true;
    }
    if (keys.contains(Qt::Key_D)) {
        cameraxyz += glm::rotateY(glm::vec3(+FLYSPEED, 0, 0), camerasph.z);
        update = true;
    }
    if (keys.contains(Qt::Key_W)) {
        cameraxyz += glm::rotateY(glm::vec3(0, 0, -FLYSPEED), camerasph.z);
        update = true;
    }
    if (keys.contains(Qt::Key_S)) {
        cameraxyz += glm::rotateY(glm::vec3(0, 0, +FLYSPEED), camerasph.z);
        update = true;
    }
    if (keys.contains(Qt::Key_F)) {
        cameraxyz += glm::vec3(0, +FLYSPEED, 0);
        update = true;
    }
    if (keys.contains(Qt::Key_R)) {
        cameraxyz += glm::vec3(0, -FLYSPEED, 0);
        update = true;
    }
    if (update) {
        updateCamera();
    }
}

void MyGL::updateCamera()
{
    int w = width();
    int h = height();

    const qreal retinaScale = devicePixelRatio();
    glViewport(0, 0, w * retinaScale, h * retinaScale);

    if (ocu) {
        // Oculus library should do all of this for us, I think
        return;
    }

    glm::mat4 proj = glm::perspective(PI * camerasph.x * 0.02f, w / (float) h, 0.05f, 200.f);
    glm::vec3 xyz = cameraxyz + glm::vec3(0, HEADHEIGHT, 0);
    glm::vec3 cam(
            xyz.x - cos(camerasph.y) * sin(camerasph.z),
            xyz.y - sin(camerasph.y),
            xyz.z - cos(camerasph.y) * cos(camerasph.z)
            );
    glm::mat4 view = glm::lookAt(xyz, cam, glm::vec3(0, 1, 0));
    glm::mat4 viewproj = proj * view;
    setViewProj(viewproj);

    printGLErrorLog();
}

void MyGL::setViewProj(const glm::mat4 &viewproj)
{
    // Upload the projection matrix
    QMatrix4x4 qviewproj = la::to_qmat(viewproj);

    prog.prog.bind();
    prog.prog.setUniformValue(prog.unifViewProj, qviewproj);
}

void MyGL::keyPressEvent(QKeyEvent *e)
{
    // http://doc.qt.io/qt-5/qt.html#Key-enum
    switch (e->key()) {
        case Qt::Key_Escape:
            captured = false;
            {
                QCursor c = cursor();
                c.setShape(Qt::ArrowCursor);
                setCursor(c);
            }
            if (e->modifiers() & Qt::ShiftModifier) {
                exit(0);
            }
            break;
        case Qt::Key_F12:
            setWindowState((Qt::WindowState)(windowState() ^ Qt::WindowFullScreen));
            updateCamera();
            break;
        case Qt::Key_Backspace:
            cameraxyz = glm::vec3(8.5, 0, 1.5);
            camerasph = glm::vec3(20, 0, 5.7);
            updateCamera();
            break;
        case Qt::Key_Comma:
            PANODISTLIMIT = glm::max(2.f, PANODISTLIMIT - 1.f);
            printf("pano distance limit: %f\n", PANODISTLIMIT);
            break;
        case Qt::Key_Period:
            PANODISTLIMIT = glm::min(50.f, PANODISTLIMIT + 1.f);
            printf("pano distance limit: %f\n", PANODISTLIMIT);
            break;
        case Qt::Key_BracketLeft:
            CLIPDIST = glm::max(2.f, CLIPDIST - 1.f);
            printf("geom clip distance: %f\n", CLIPDIST);
            break;
        case Qt::Key_BracketRight:
            CLIPDIST = glm::min(50.f, CLIPDIST + 1.f);
            printf("geom clip distance: %f\n", CLIPDIST);
            break;
        case Qt::Key_9:
            SMARTDIST = glm::max(-5.f, SMARTDIST - 0.2f);
            printf("geom smart distance: %f\n", SMARTDIST);
            break;
        case Qt::Key_0:
            SMARTDIST = glm::min(20.f, SMARTDIST + 0.2f);
            printf("geom smart distance: %f\n", SMARTDIST);
            break;
        case Qt::Key_O:
            if (ocu) {
                delete ocu;
                ocu = nullptr;
            } else {
                ocu = new Oculus();
            }
            updateCamera();
            break;
#if TIMING
        case Qt::Key_T:
            rendertiming ^= true;
            if (!rendertiming) {
                printf("\n");
            }
            break;
#endif
        default:
            keys.insert((Qt::Key) e->key());
            break;
    }
}

void MyGL::keyReleaseEvent(QKeyEvent *e)
{
    keys.remove((Qt::Key) e->key());
}

void MyGL::mousePressEvent(QMouseEvent *)
{
    QPoint mid = QPoint(width() / 2, height() / 2);
    QCursor c = cursor();
    c.setPos(mapToGlobal(mid));
    c.setShape(Qt::BlankCursor);
    setCursor(c);

    captured = true;
}

void MyGL::mouseMoveEvent(QMouseEvent *e)
{
    if (captured) {
        QPoint mid = QPoint(width() / 2, height() / 2);

        auto pos = e->pos();
        QPoint diff = pos - mid;

        QCursor c = cursor();
        c.setPos(mapToGlobal(mid));
        c.setShape(Qt::BlankCursor);
        setCursor(c);

        camerasph = glm::vec3(camerasph.x,
                              glm::clamp(camerasph.y - diff.y() * 0.002f, -0.4999f * PI, 0.4999f * PI),
                              glm::mod(camerasph.z - diff.x() * 0.002f, 2 * PI));
        updateCamera();
    }
}

void MyGL::wheelEvent(QWheelEvent *e)
{
    auto delta = e->angleDelta();
    if (!delta.isNull()) {
        camerasph = glm::vec3(glm::clamp(camerasph.x + delta.y() * -0.005f, 2.f, 30.f), camerasph.y, camerasph.z);
        updateCamera();
    }
}
