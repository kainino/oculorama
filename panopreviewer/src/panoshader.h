#pragma once

#include <utils.h>

#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>

class PanoShader
{
public:
    class Drawable
    {
    public:
        virtual ~Drawable() { }
        virtual GLenum drawMode() = 0;
        virtual int elemCount() = 0;
        virtual bool bindIdx() = 0;
        virtual bool bindTexCoord() = 0;
        virtual void bindDepthTex(GLuint unit) = 0;
        virtual void bindColorTex(GLuint unit) = 0;
    };

public:
    QOpenGLShaderProgram prog;

    int attrTexCoord;

    int unifModel;
    int unifViewProj;
    int unifAspect;
    int unifPanoDepth;
    int unifPanoColor;
    int unifCamPos;
    int unifClipDist;
    int unifSmartDist;
    int unifCenter;

public:
    void create(const char *vertfile, const char *geomfile, const char *fragfile);
    void draw(QOpenGLFunctions &f, Drawable &d, const glm::vec3 &campos, float clipdist, float smartdist);
};
