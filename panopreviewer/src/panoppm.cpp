#include "panoppm.h"

#include <cassert>
#include <QFile>
#include <QImage>


static QByteArray readWord(QFile &f)
{
    char b = 0;

    // Clear through everything that is whitespace
    do {
        auto bs = f.read(1);
        if (bs.length()) {
            b = bs[0];
        } else {
            break;
        }
    } while(isspace(b));

    // Read everything that isn't whitespace
    QByteArray segment;
    do {
        segment.append(b);

        auto bs = f.read(1);
        if (bs.length()) {
            b = bs[0];
        } else {
            break;
        }
    } while(!isspace(b));

    return segment;
}

PanoPPM::PanoPPM(const QString &filename, float permeter)
{
    QFile f(filename);
    if (!f.open(QFile::ReadOnly)) {
        printf("Depth PPM loading failed\n");
        exit(EXIT_FAILURE);
    }


    auto p6 = QString(readWord(f));
    assert(p6 == "P6");
    _width = QString(readWord(f)).toInt();
    _height = QString(readWord(f)).toInt();
    int maxval = QString(readWord(f)).toInt();
    int pxs = _width * _height;
    float tometers = 1 / permeter;

    // Read last whitespace
    f.read(1);

    _data.resize(pxs);
    if (maxval < 256) {
        QByteArray im = f.read(pxs * 3);
        for (int i = 0; i < pxs; i++) {
            unsigned char v = im.at(3 * i);
            _data[i] = v * tometers;
        }
    } else {
        QByteArray im = f.read(pxs * 3 * 2);
        for (int i = 0; i < pxs; i++) {
            unsigned char v1 = im.at(6 * i);
            unsigned char v2 = im.at(6 * i + 1);
            unsigned short v = v1 + v2 * 256;
            _data[i] = v * tometers;
        }
    }
}

