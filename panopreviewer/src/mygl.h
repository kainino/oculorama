#pragma once

#include <utils.h>
#include <oculus.h>

#include <openglwindow.h>
#include <panoshader.h>
#include <panorama.h>
#include <panoset.h>

#include <vector>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QSet>
#include <QTime>

#define TIMING 1


class MyGL
    : public OpenGLWindow
{
private:
    QOpenGLVertexArrayObject vao;

    Oculus *ocu;
    PanoSet panos;
    PanoShader prog;

    float PANODISTLIMIT = 6;
    float CLIPDIST = 14;
    float SMARTDIST = 1;
    la::vec3 cameraxyz;
    la::vec3 camerasph;
    bool captured;

#if TIMING
    QTime lastframe;
    bool rendertiming;
#endif

    QSet<Qt::Key> keys;

public:
    MyGL();
    ~MyGL();

    void initialize();
    void render();
    void renderFrame();

protected:
    void resizeEvent(QResizeEvent *);

    void updateKeys();
    void updateCamera();
    void setViewProj(const glm::mat4 &viewproj);

    void keyPressEvent(QKeyEvent *e);
    void keyReleaseEvent(QKeyEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void wheelEvent(QWheelEvent *e);
};
