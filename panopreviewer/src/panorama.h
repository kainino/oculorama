#pragma once

#include <utils.h>
#include <panoshader.h>

#include <QMap>
#include <QPair>
#include <QOpenGLTexture>



class Panorama
    : public PanoShader::Drawable
{
private:
    struct Bufs {
        GLuint count;
        QOpenGLBuffer idx;
        QOpenGLBuffer txc;
    };

private:
    QOpenGLTexture texDepth;
    QOpenGLTexture texColor;

    glm::vec3 _translation;
    glm::mat4 modelmat;
    float aspect;
    QPair<int, int> dim;
    Bufs *bufs;

    static QMap<QPair<int, int>, Bufs> buffers;

public:

    Panorama();
    ~Panorama();

    void create();
    void destroy();

    void loadJSON(const QJsonObject &json, const QString &basepath);
    void setUniforms(PanoShader &ps);

    GLenum drawMode();
    int elemCount();
    bool bindIdx();
    bool bindTexCoord();
    void bindDepthTex(GLuint unit);
    void bindColorTex(GLuint unit);

    glm::vec3 translation() {
        return _translation;
    }
};
