#version 150

uniform mat4 Model;
uniform vec2 LimPitch;
uniform float Aspect;
uniform sampler2D PanoDepth;

in vec2 vs_TexCoord;

out VertexData {
    vec2 tc;
    float d;
} VertexOut;

const float PI = 3.1415926535897932384626433832795;

float remap(float t, vec2 tlim, vec2 ulim)
{
    return (t - tlim.x) / (tlim.y - tlim.x) * (ulim.y - ulim.x) + ulim.x;
}

vec4 where(vec2 tc, float depth)
{
    float   yaw    = remap(tc.x, vec2(0.0, 1.0), vec2(0, 2 * PI));
    float maxPitch = PI * Aspect;
    float pitch    = remap(tc.y, vec2(0.0, 1.0), vec2(-maxPitch, maxPitch));

    vec3 dir = vec3(
            cos(pitch) * cos(yaw),
            -sin(pitch),
            cos(pitch) * sin(yaw));

    return vec4(depth * dir, 1);
}

void main()
{
    vec2 tc = vs_TexCoord;
    float d = texture(PanoDepth, tc).x;

    gl_Position = Model * where(tc, d);
    VertexOut.tc = tc;
    VertexOut.d = d;
}
