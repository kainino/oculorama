#version 150

#define WIRE 0

layout(triangles) in;
#if WIRE
layout(line_strip, max_vertices = 4) out;
#else
layout(triangle_strip, max_vertices = 3) out;
#endif

uniform vec3 Center;
uniform vec3 CamPos;
uniform float ClipDist;
uniform float SmartDist;
uniform mat4 ViewProj;

in VertexData {
    vec2 tc;
    float d;
} VertexIn[3];

out VertexData {
    vec2 tc;
} VertexOut;


const vec3 LIGHT = vec3(0, 2, 0);
const float LIMIT = 0.04;
const float MAXDEPTH = 4.0;


void main()
{
    vec3 upave = (gl_in[0].gl_Position.xyz + gl_in[1].gl_Position.xyz + gl_in[2].gl_Position.xyz) / 3.0;

    vec4 p0 = ViewProj * gl_in[0].gl_Position;
    vec4 p1 = ViewProj * gl_in[1].gl_Position;
    vec4 p2 = ViewProj * gl_in[2].gl_Position;
    vec3 pave = (p0.xyz + p1.xyz + p2.xyz) / 3.0;

    if (pave.z < 0) return;
    if (length(pave) > ClipDist) return;

    float d0 = VertexIn[0].d;
    float d1 = VertexIn[1].d;
    float d2 = VertexIn[2].d;
    float dave = (d0 + d1 + d2) / 3;
    //if (dave - distance(upave, CamPos) > SmartDist) return;

    bool tri1 = abs(d0 - d1) < LIMIT * dave && abs(d1 - d2) < LIMIT * dave;
    if (!tri1) return;

    //vec3 n = cross(gl_in[2].gl_Position.xyz - gl_in[1].gl_Position.xyz, gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz);
    gl_Position = p0; VertexOut.tc = VertexIn[0].tc; EmitVertex();
    gl_Position = p1; VertexOut.tc = VertexIn[1].tc; EmitVertex();
    gl_Position = p2; VertexOut.tc = VertexIn[2].tc; EmitVertex();
#if WIRE
    gl_Position = p0; VertexOut.tc = VertexIn[0].tc; EmitVertex();
#endif
    EndPrimitive();
}
