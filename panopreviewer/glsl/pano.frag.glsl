#version 150

uniform sampler2D PanoColor;

in VertexData {
    vec2 tc;
} VertexIn;

out vec4 out_Col;

void main()
{
    out_Col = texture(PanoColor, VertexIn.tc);
}
