#pragma once

#include "glutil.h"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include "OVR_CAPI.h"
#include "OVR_CAPI_GL.h"
#include "Kernel/OVR_Math.h"


/**
 * Largely copied from http://nuclear.mutantstargoat.com/hg/oculus2/
 */
class Oculus
{
private:
    bool debug;
    ovrHmd hmd;
    ovrFrameTiming frame_timing;
    ovrSizei eye_res[2];
    ovrGLTexture fb_ovr_tex[2];
    ovrGLConfig glcfg;
    ovrEyeRenderDesc eye_rdesc[2];
    unsigned int hmd_caps, distort_caps;
    ovrPosef pose[2];

    int fb_width, fb_height;
    int fb_tex_width, fb_tex_height;
    GLuint fbo, fb_tex, fb_depth;

public:
    Oculus(GLFWwindow *window);

    void destroy();

    void begin_frame();
    void end_frame();
    void begin_eye(int i_eye, const glm::mat4 &headrot,
            glm::mat4 &proj, glm::mat4 &view);

    void update_render_target();

    void dismiss_hsw();

    void debug_print();
    void debug_print_loop();
};
