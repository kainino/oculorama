#pragma once

#include "oculus.h"

namespace Render
{
    extern GLFWwindow *window;
    extern Oculus *ocu;
    extern float pitch;
    extern float yaw;

    void init();

    void destroy();

    void main_loop();

    void render_frame(int width, int height,
            const glm::mat4 &proj, const glm::mat4 &view);

    void error_callback(int error, const char *desc);

    void key_callback(GLFWwindow *window,
            int key, int scancode, int action, int mods);

    void resize_callback(GLFWwindow *window, int width, int height);
};
