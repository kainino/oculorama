#include "oculus.h"
#include "render.h"

int main(int argc, char *argv[])
{
    Render::init();
    Oculus ocu(Render::window);
    Render::ocu = &ocu;

    Render::main_loop();

    //ocu.debug_print_loop();

    ocu.destroy();
    Render::destroy();
}
