#pragma once

#ifdef WIN32
#    define OVR_OS_WIN32
#elif defined(__APPLE__)
#    define OVR_OS_MAC
#else
#    define OVR_OS_LINUX
#endif


inline unsigned int next_pow2(unsigned int x)
{
    x -= 1;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return x + 1;
}
