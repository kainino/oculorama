#include "oculus_native.h"

#include <cstring>

#if defined(OVR_OS_WIN32)
#    define GLFW_EXPOSE_NATIVE_WIN32
#    define GLFW_EXPOSE_NATIVE_WGL
#elif defined(OVR_OS_MAC)
#    define GLFW_EXPOSE_NATIVE_COCOA
#    define GLFW_EXPOSE_NATIVE_NSGL
#elif defined(OVR_OS_LINUX)
#    define GLFW_EXPOSE_NATIVE_X11
#    define GLFW_EXPOSE_NATIVE_GLX
#endif

#include <GLFW/glfw3native.h>


void createGLConfig(ovrHmd &hmd, ovrGLConfig &glcfg, GLFWwindow *window)
{
    memset(&glcfg, 0, sizeof(glcfg));
    glcfg.OGL.Header.API = ovrRenderAPI_OpenGL;
    glcfg.OGL.Header.BackBufferSize = hmd->Resolution;
    glcfg.OGL.Header.Multisample = 1;
#if defined(OVR_OS_WIN32)
    //glcfg.OGL.WglContext = glfwGetWGLContext(window);
    glcfg.OGL.Window = glfwGetWin32Window(window);
    // Don't know which of these to use
    glcfg.OGL.DC = GetDC(glcfg.OGL.Window);
#elif defined(OVR_OS_MAC)
    // Don't know which of these to use
    glcfg.OGL.Window = glfwGetCocoaWindow(window);
    //glcfg.OGL.Window = glfwGetCocoaWindow(window);
#elif defined(OVR_OS_LINUX)
    glcfg.OGL.Disp = glfwGetX11Display();
    //glcfg.OGL.Window = glfwGetX11Window(window);
#endif
}
