#include <cstdlib>
#include <cstdio>

#define GLM_FORCE_RADIANS
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/euler_angles.hpp>

#include "render.h"

GLFWwindow *Render::window = NULL;
Oculus     *Render::ocu = NULL;
float       Render::pitch = 0;
float       Render::yaw = 0;

void Render::init()
{
    //glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    //glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    glfwSetErrorCallback(error_callback);
    if (!glfwInit()) {
        exit(EXIT_FAILURE);
    }

    window = glfwCreateWindow(1920, 1080, "", glfwGetPrimaryMonitor(), NULL);
    if (!window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, key_callback);

    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "glewInit failure\n");
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glClearColor(0.5, 0.5, 0.5, 1.0);
}

void Render::destroy()
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

void Render::main_loop()
{
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    while (!glfwWindowShouldClose(window)) {
        ocu->dismiss_hsw();

        glm::mat4 proj, view;

        ocu->begin_frame();

        glm::mat4 headrot = glm::eulerAngleXY(pitch, yaw);

        ocu->begin_eye(0, headrot, proj, view);
        render_frame(width, height, proj, view);

        ocu->begin_eye(1, headrot, proj, view);
        render_frame(width, height, proj, view);

        ocu->end_frame();

        glfwPollEvents();
    }
}

void Render::render_frame(int width, int height,
        const glm::mat4 &proj, const glm::mat4 &view)
{
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(&proj[0][0]);

    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(&view[0][0]);

    glEnable(GL_DEPTH_TEST);
    glPointSize(50.0);
    glBegin(GL_POINTS);
    {
        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex3f(0.0f, 0.0f, 0.0f);
    }
    glEnd();

    // x axis loop
    glBegin(GL_LINE_LOOP);
    {
        glColor3f(0, 0, 0);
        glVertex3f(0.0f, -10.0f, -10.0f);
        glColor3f(1, 0, 0);
        glVertex3f(0.0f, -10.0f, +10.0f);
        glVertex3f(0.0f, +10.0f, +10.0f);
        glVertex3f(0.0f, +10.0f, -10.0f);
    }
    glEnd();

    // y axis loop
    glBegin(GL_LINE_LOOP);
    {
        glColor3f(0, 0, 0);
        glVertex3f(-10.0f, 0.0f, -10.0f);
        glColor3f(0, 1, 0);
        glVertex3f(-10.0f, 0.0f, +10.0f);
        glVertex3f(+10.0f, 0.0f, +10.0f);
        glVertex3f(+10.0f, 0.0f, -10.0f);
    }
    glEnd();

    // z axis loop
    glBegin(GL_LINE_LOOP);
    {
        glColor3f(0, 0, 0);
        glVertex3f(-10.0f, -10.0f, 0.0f);
        glColor3f(0, 0, 1);
        glVertex3f(-10.0f, +10.0f, 0.0f);
        glVertex3f(+10.0f, +10.0f, 0.0f);
        glVertex3f(+10.0f, -10.0f, 0.0f);
    }
    glEnd();
}

void Render::error_callback(int error, const char *desc)
{
    fprintf(stderr, "GLFW Error %d: %s\n", error, desc);
}

void Render::key_callback(GLFWwindow *window,
        int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    } else if (action == GLFW_PRESS || action == GLFW_REPEAT) {
        const float anglestep = 3.14159f / 16;
        if (key == GLFW_KEY_DOWN) {
            pitch += anglestep;
        } else if (key == GLFW_KEY_UP) {
            pitch -= anglestep;
        } else if (key == GLFW_KEY_RIGHT) {
            yaw += anglestep;
        } else if (key == GLFW_KEY_LEFT) {
            yaw -= anglestep;
        }
    }
}
